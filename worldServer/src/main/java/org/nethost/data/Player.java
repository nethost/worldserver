package org.nethost.data;

public class Player {
	private String id;
	private Vector3 position;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Vector3 getPosition() {
		return position;
	}

	public void setPosition(Vector3 position) {
		this.position = position;
	}

}
