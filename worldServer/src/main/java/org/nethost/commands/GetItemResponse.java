package org.nethost.commands;

import org.nethost.data.Item;
import org.nethost.database.objects.ItemsDatabase;
import org.nethost.interfaces.Iresponse;

public class GetItemResponse implements Iresponse{
	private String command;
	private String result;
	private Item item;
	
	public GetItemResponse(GetItemCommand cmd) {
		ItemsDatabase id = new ItemsDatabase();
		item = id.getItem(cmd.getId());
		command = cmd.getCommand();
		result = "OK";
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

}
