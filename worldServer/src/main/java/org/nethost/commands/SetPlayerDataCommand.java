package org.nethost.commands;

import org.nethost.data.PlayerData;
import org.nethost.interfaces.Icommand;

public class SetPlayerDataCommand implements Icommand{
	private String command;	
	private PlayerData playerData;
	private String uid;

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}


	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public PlayerData getPlayerData() {
		return playerData;
	}

	public void setPlayerData(PlayerData playerData) {
		this.playerData = playerData;
	}


}
