package org.nethost.commands;

import org.nethost.data.GameCharacter;
import org.nethost.database.objects.GameCharacterDatabase;
import org.nethost.interfaces.Iresponse;

public class GetCharacterResponse implements Iresponse{
	private String command;
	private String result;
	private GameCharacter character;
	
	public GetCharacterResponse(GetCharacterCommand cmd) {		
		command = cmd.getCommand();
		
		try {
			GameCharacterDatabase gcd = new GameCharacterDatabase();
			character = gcd.getCharacter(cmd.getId());
			result = "OK";
		} catch (Exception ex){
			result = ex.getMessage();
			character = null;
		}	
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public GameCharacter getCharacter() {
		return character;
	}

	public void setCharacter(GameCharacter character) {
		this.character = character;
	}

}
