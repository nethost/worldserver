package org.nethost.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.nethost.data.Item;
import org.nethost.globals.Variables;
import org.nethost.interfaces.Iresponse;

public class GetItemsResponse implements Iresponse{
	private String command;
	private String result;	
	private ArrayList<Item> items = new ArrayList<>();

	public GetItemsResponse(GetItemsCommand cmd) {	
		command = cmd.getCommand();
		
		for (Entry<Integer, Item> entry : Variables.getItemCache().entrySet()) {
			items.add(entry.getValue());
		}
		result = "OK";
	}
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> entities) {
		this.items = (ArrayList<Item>) entities;
	}
}
