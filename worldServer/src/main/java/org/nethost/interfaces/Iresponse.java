package org.nethost.interfaces;

public interface Iresponse {
	public String getCommand();
	public void setCommand(String command);
	public String getResult();
	public void setResult(String result);
}
