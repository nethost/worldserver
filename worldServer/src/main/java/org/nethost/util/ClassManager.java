package org.nethost.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.nethost.database.Database;

//big thanks https://dzone.com/articles/get-all-classes-within-package
public class ClassManager {
	private ClassManager() {

	}

	/**
	 * Scans all classes accessible from the context class loader which belong to
	 * the given package and subpackages.
	 *
	 * @param packageName
	 *            The base package
	 * @return The classes
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public static Class<Database>[] getClasses(String packageName) throws ClassNotFoundException, IOException {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		assert classLoader != null;
		String path = packageName.replace('.', '/');
		Enumeration<?> resources = classLoader.getResources(path);
		List<File> dirs = new ArrayList<>();
		while (resources.hasMoreElements()) {
			URL resource = (URL) resources.nextElement();
			dirs.add(new File(resource.getFile()));
		}
		ArrayList<Class<?>> classes = new ArrayList<>();
		for (File directory : dirs) {
			classes.addAll(findClasses(directory, packageName));
		}
		return (Class<Database>[]) classes.toArray(new Class[classes.size()]);
	}

	/**
	 * Recursive method used to find all classes in a given directory and subdirs.
	 * 
	 * 2018-04-20 Corrected by Martin. Also works with JAR files.
	 *
	 * @param directory
	 *            The base directory
	 * @param packageName
	 *            The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	private static List<Class<?>> findClasses(File directory, String packageName)
			throws ClassNotFoundException, IOException {
		List<Class<?>> classes = new ArrayList<>();
		// check if we have jar
		boolean isJar = false;
		String[] pathSplitted = directory.getPath().split("!");

		if (pathSplitted.length > 1) {
			// is jar
			isJar = true;
		}

		if (isJar) {
			IOException ex = null;

			try (ZipInputStream zip = new ZipInputStream(
					new FileInputStream(pathSplitted[0].replaceAll("file:\\\\", "")))) {

				String packageToFind = packageName.replace(".", "/");
				for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
					if (entry.getName().contains(packageToFind) && entry.getName().endsWith(".class")) {
						String[] classSplit = entry.getName().split("/");
						String className = classSplit[classSplit.length - 1];
						classes.add(Class.forName(packageName + '.' + className.substring(0, className.length() - 6)));
					}
				}
			} catch (IOException exception) {
				ex = exception;
			}

			if (ex != null) {
				throw ex;
			}

			return classes;
		}

		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				assert !file.getName().contains(".");
				classes.addAll(findClasses(file, packageName + "." + file.getName()));
			} else if (file.getName().endsWith(".class")) {
				classes.add(
						Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
			}
		}
		return classes;
	}
}
