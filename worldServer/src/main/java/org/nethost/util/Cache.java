package org.nethost.util;

import java.util.HashMap;

import org.nethost.data.GameObject;
import org.nethost.data.GameObjectSpawn;
import org.nethost.data.Item;
import org.nethost.database.objects.GameObjectDatabase;
import org.nethost.database.objects.GameObjectSpawnDatabase;
import org.nethost.database.objects.ItemsDatabase;
import org.nethost.globals.Variables;
import org.nethost.gui.MainGui;

public class Cache {
	private Cache() {
		
	}
	
	public static void clearCache() {
		Variables.setItemCache(new HashMap<Integer, Item>());
		Variables.setGameObjectCache(new HashMap<Integer, GameObject>());
		Variables.setGameObjectSpawnCache(new HashMap<Integer, GameObjectSpawn>());
	}
	
	public static void reloadCache() {		
		clearCache();
		
		ItemsDatabase itemsDatabase = new ItemsDatabase();
		itemsDatabase.fillItemCache();
		MainGui.writeLogSuccess(Variables.getItemCache().size() + " items loaded");

		GameObjectDatabase goDatabase = new GameObjectDatabase();
		goDatabase.fillGameObjectCache();
		MainGui.writeLogSuccess(Variables.getGameObjectCache().size() + " gameObjects loaded");

		GameObjectSpawnDatabase gosDatabase = new GameObjectSpawnDatabase();
		gosDatabase.fillGameObjectSpawnCache();
		MainGui.writeLogSuccess(Variables.getGameObjectSpawnCache().size() + " gameObjectsSpawns loaded");		
	}
}
