package org.nethost.globals;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nethost.data.GameObject;
import org.nethost.data.GameObjectSpawn;
import org.nethost.data.Item;
import org.nethost.data.PlayerData;
import org.nethost.gui.MainGui;
import org.quickserver.net.server.ClientHandler;
import org.quickserver.net.server.QuickServer;

public class Variables {
	private Variables() {

	}

	private static boolean isEditorMode = false;

	private static int clientVersion = 1;

	private static int versionMajor = 0;
	private static int versionMinor = 1;
	private static int versionBuild = 2;

	private static Map<String, ClientHandler> connectedClients = null;

	// cache
	private static Map<Integer, Item> itemCache = null;
	private static Map<Integer, GameObject> gameObjectCache = null;
	private static Map<Integer, GameObjectSpawn> gameObjectSpawnCache = null;

	private static QuickServer qs = null;

	private static List<String> usedCommands = new ArrayList<>();

	private static MainGui mainGui;

	private static boolean serverRunning = false;

	private static boolean useCompression = false;

	private static Map<String, String> clientNameToUID = new HashMap<>();
	private static Map<String, Integer> authenticatedPlayers = new HashMap<>();

	private static Map<String, PlayerData> playerData = new HashMap<>();

	// Crypring related

	private static String aesKey = "";

	// Logging related

	private static boolean enableWriteLog = true;
	private static Color writeLogColor = Color.BLACK;

	private static boolean enableWriteLogSuccess = true;
	private static Color writeLogSuccessColor = Color.GREEN;

	private static boolean enableWriteLogConsole = true;
	private static Color writeLogConsoleColor = Color.MAGENTA;

	private static boolean enableWriteLogError = true;
	private static Color writeLogErrorColor = Color.RED;

	private static boolean enableWriteLogDebug = true;
	private static Color writeLogDebugColor = Color.DARK_GRAY;

	private static boolean enableWriteLogInfo = true;
	private static Color writeLogInfoColor = Color.BLUE;

	public static boolean isEditorMode() {
		return isEditorMode;
	}

	public static void setEditorMode(boolean isEditorMode) {
		Variables.isEditorMode = isEditorMode;
	}

	public static int getClientVersion() {
		return clientVersion;
	}

	public static void setClientVersion(int clientVersion) {
		Variables.clientVersion = clientVersion;
	}

	public static int getVersionMajor() {
		return versionMajor;
	}

	public static void setVersionMajor(int versionMajor) {
		Variables.versionMajor = versionMajor;
	}

	public static int getVersionMinor() {
		return versionMinor;
	}

	public static void setVersionMinor(int versionMinor) {
		Variables.versionMinor = versionMinor;
	}

	public static int getVersionBuild() {
		return versionBuild;
	}

	public static void setVersionBuild(int versionBuild) {
		Variables.versionBuild = versionBuild;
	}

	public static Map<String, ClientHandler> getConnectedClients() {
		return connectedClients;
	}

	public static void setConnectedClients(Map<String, ClientHandler> connectedClients) {
		Variables.connectedClients = connectedClients;
	}

	public static Map<Integer, Item> getItemCache() {
		return itemCache;
	}

	public static void setItemCache(Map<Integer, Item> itemCache) {
		Variables.itemCache = itemCache;
	}

	public static Map<Integer, GameObject> getGameObjectCache() {
		return gameObjectCache;
	}

	public static void setGameObjectCache(Map<Integer, GameObject> gameObjectCache) {
		Variables.gameObjectCache = gameObjectCache;
	}

	public static Map<Integer, GameObjectSpawn> getGameObjectSpawnCache() {
		return gameObjectSpawnCache;
	}

	public static void setGameObjectSpawnCache(Map<Integer, GameObjectSpawn> gameObjectSpawnCache) {
		Variables.gameObjectSpawnCache = gameObjectSpawnCache;
	}

	public static QuickServer getQs() {
		return qs;
	}

	public static void setQs(QuickServer qs) {
		Variables.qs = qs;
	}

	public static List<String> getUsedCommands() {
		return usedCommands;
	}

	public static void setUsedCommands(List<String> usedCommands) {
		Variables.usedCommands = usedCommands;
	}

	public static MainGui getMainGui() {
		return mainGui;
	}

	public static void setMainGui(MainGui mainGui) {
		Variables.mainGui = mainGui;
	}

	public static boolean isServerRunning() {
		return serverRunning;
	}

	public static void setServerRunning(boolean serverRunning) {
		Variables.serverRunning = serverRunning;
	}

	public static boolean isUseCompression() {
		return useCompression;
	}

	public static void setUseCompression(boolean useCompression) {
		Variables.useCompression = useCompression;
	}

	public static Map<String, String> getClientNameToUID() {
		return clientNameToUID;
	}

	public static void setClientNameToUID(Map<String, String> clientNameToUID) {
		Variables.clientNameToUID = clientNameToUID;
	}

	public static Map<String, Integer> getAuthenticatedPlayers() {
		return authenticatedPlayers;
	}

	public static void setAuthenticatedPlayers(Map<String, Integer> authenticatedPlayers) {
		Variables.authenticatedPlayers = authenticatedPlayers;
	}

	public static Map<String, PlayerData> getPlayerData() {
		return playerData;
	}

	public static void setPlayerData(Map<String, PlayerData> playerData) {
		Variables.playerData = playerData;
	}

	public static String getAESKey() {
		return aesKey;
	}

	public static void setAESKey(String aesKeyIn) {
		aesKey = aesKeyIn;
	}

	public static boolean isEnableWriteLog() {
		return enableWriteLog;
	}

	public static void setEnableWriteLog(boolean enableWriteLog) {
		Variables.enableWriteLog = enableWriteLog;
	}

	public static Color getWriteLogColor() {
		return writeLogColor;
	}

	public static void setWriteLogColor(Color writeLogColor) {
		Variables.writeLogColor = writeLogColor;
	}

	public static boolean isEnableWriteLogSuccess() {
		return enableWriteLogSuccess;
	}

	public static void setEnableWriteLogSuccess(boolean enableWriteLogSuccess) {
		Variables.enableWriteLogSuccess = enableWriteLogSuccess;
	}

	public static Color getWriteLogSuccessColor() {
		return writeLogSuccessColor;
	}

	public static void setWriteLogSuccessColor(Color writeLogSuccessColor) {
		Variables.writeLogSuccessColor = writeLogSuccessColor;
	}

	public static boolean isEnableWriteLogConsole() {
		return enableWriteLogConsole;
	}

	public static void setEnableWriteLogConsole(boolean enableWriteLogConsole) {
		Variables.enableWriteLogConsole = enableWriteLogConsole;
	}

	public static Color getWriteLogConsoleColor() {
		return writeLogConsoleColor;
	}

	public static void setWriteLogConsoleColor(Color writeLogConsoleColor) {
		Variables.writeLogConsoleColor = writeLogConsoleColor;
	}

	public static boolean isEnableWriteLogError() {
		return enableWriteLogError;
	}

	public static void setEnableWriteLogError(boolean enableWriteLogError) {
		Variables.enableWriteLogError = enableWriteLogError;
	}

	public static Color getWriteLogErrorColor() {
		return writeLogErrorColor;
	}

	public static void setWriteLogErrorColor(Color writeLogErrorColor) {
		Variables.writeLogErrorColor = writeLogErrorColor;
	}

	public static boolean isEnableWriteLogDebug() {
		return enableWriteLogDebug;
	}

	public static void setEnableWriteLogDebug(boolean enableWriteLogDebug) {
		Variables.enableWriteLogDebug = enableWriteLogDebug;
	}

	public static Color getWriteLogDebugColor() {
		return writeLogDebugColor;
	}

	public static void setWriteLogDebugColor(Color writeLogDebugColor) {
		Variables.writeLogDebugColor = writeLogDebugColor;
	}

	public static boolean isEnableWriteLogInfo() {
		return enableWriteLogInfo;
	}

	public static void setEnableWriteLogInfo(boolean enableWriteLogInfo) {
		Variables.enableWriteLogInfo = enableWriteLogInfo;
	}

	public static Color getWriteLogInfoColor() {
		return writeLogInfoColor;
	}

	public static void setWriteLogInfoColor(Color writeLogInfoColor) {
		Variables.writeLogInfoColor = writeLogInfoColor;
	}
}
