package org.nethost.globals;

import javax.swing.SwingUtilities;

import org.nethost.gui.MainGui;
import org.nethost.util.DebugStream;

public class StartupEditor {
	public static void main(String[] args) {
		DebugStream.activate();
		Variables.setEditorMode(true);

		MainGui gui = new MainGui();

		SwingUtilities.invokeLater(() -> gui.setVisible(true));

		Variables.setMainGui(gui);

		Server s = new Server();
		s.startMySql();

		MainGui.writeLogInfo("Editor mode!");
	}

}
