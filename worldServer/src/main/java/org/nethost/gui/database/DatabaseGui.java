package org.nethost.gui.database;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.EmptyBorder;

import org.nethost.database.Database;
import org.nethost.database.SqlCommands;
import org.nethost.gui.MainGui;
import org.nethost.util.Cache;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.swing.JButton;
import java.awt.Component;
import java.awt.Point;

public class DatabaseGui extends JFrame {
	private static final long serialVersionUID = 8201769457512742749L;
	private JPanel contentPane;
	private JTable table;
	private JTextField txtSearch;
	private JLabel lblSearch;
	private transient Database dataBase;

	public DatabaseGui(Object obj) {
		dataBase = (Database) obj;

		contentPane = new JPanel();
		table = new JTable();
		JScrollPane scrollPane = new JScrollPane();
		JPopupMenu popupMenu = new JPopupMenu();
		JMenuItem menuItemEdit = new JMenuItem("Edit Current Row");
		JMenuItem menuItemRemove = new JMenuItem("Remove Current Row");
		txtSearch = new JTextField();
		lblSearch = new JLabel("Search");

		setType(Type.UTILITY);
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 888, 364);
		setContentPane(contentPane);
		setTitle(dataBase.getTableName().replaceAll("_", " "));

		scrollPane.setBounds(0, 36, 882, 300);
		scrollPane.setViewportView(table);

		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		contentPane.add(scrollPane);
		contentPane.add(txtSearch);
		contentPane.add(lblSearch);

		popupMenu.add(menuItemEdit);
		popupMenu.add(menuItemRemove);

		table.setComponentPopupMenu(popupMenu);
		table.setModel(new DataTableModel(dataBase.getColumNames(), dataBase.getTableData()));
		table.setDragEnabled(false);
		table.getTableHeader().setReorderingAllowed(false);

		txtSearch.setBounds(58, 5, 107, 20);
		txtSearch.setColumns(10);

		lblSearch.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSearch.setBounds(2, 8, 46, 14);

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent event) {
				// selects the row at which point the mouse is clicked
				Point point = event.getPoint();
				int currentRow = table.rowAtPoint(point);
				table.setRowSelectionInterval(currentRow, currentRow);
			}
		});

		menuItemEdit.addActionListener(e -> {

			int row = table.getSelectedRow();
			int id = (int) table.getValueAt(row, 0);

			setEnabled(false);
			JPanel contentPaneWindow;

			JFrame popupWindow = new JFrame();

			popupWindow.setType(Type.UTILITY);
			popupWindow.setResizable(false);
			popupWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

			popupWindow.setTitle("Edit " + dataBase.getTableName().replaceAll("_", " "));

			popupWindow.setBounds(100, 100, 251, 446);

			popupWindow.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent arg0) {
					setEnabled(true);
				}
			});

			contentPaneWindow = new JPanel();
			contentPaneWindow.setBorder(new EmptyBorder(5, 5, 5, 5));
			popupWindow.setContentPane(contentPaneWindow);
			contentPaneWindow.setLayout(null);

			JButton btnSave = new JButton("Save");
			btnSave.setBounds(5, 383, 235, 23);

			btnSave.addActionListener(e1 -> {

				Component[] cmp = contentPaneWindow.getComponents();

				HashMap<String, Object> insert = new HashMap<>();

				for (Component comp : cmp) {
					try {
						if (comp.getName().startsWith("txt_") && ((JTextField) comp).getText().length() > 0) {
							insert.put(comp.getName().substring(4), ((JTextField) comp).getText());
						}
					} catch (NullPointerException ex) {
						MainGui.writeLogError("ex " + ex.getMessage());
					}
				}

				SqlCommands sql = new SqlCommands();
				try {
					sql.sqlUpdate(dataBase.getTableName(), insert, "id = " + id);
				} catch (SQLException ex) {
					MainGui.writeLogError("ex " + ex.getMessage());
				}

				redrawTable();
				setEnabled(true);
				popupWindow.dispose();

			});

			contentPaneWindow.add(btnSave);

			LinkedHashMap<String, Object> fields = (LinkedHashMap<String, Object>) dataBase.getColumNames();

			int newRow = 22;
			int currentPos = 11;

			int tablePos = 0;

			for (String entry : fields.keySet()) {
				String value = String.valueOf(table.getValueAt(row, tablePos));
				tablePos++;

				if (entry.equals("id")) {
					continue;
				}

				JLabel lblNewLabel = new JLabel(entry);
				lblNewLabel.setBounds(5, currentPos, 92, 14);
				contentPaneWindow.add(lblNewLabel);

				JTextField textField = new JTextField();
				textField.setName("txt_" + entry);
				if (!value.equals("null")) {
					textField.setText(value);
				}
				textField.setBounds(120, currentPos - 3, 120, 20);
				contentPaneWindow.add(textField);
				textField.setColumns(10);

				currentPos += newRow;
			}
			popupWindow.setVisible(true);

			redrawTable();

		});

		menuItemRemove.addActionListener(e -> {
			int row = table.getSelectedRow();
			int id = (int) table.getValueAt(row, 0);

			SqlCommands sql = new SqlCommands();
			try {
				sql.sqlDelete(dataBase.getTableName(), "id=" + id);
			} catch (SQLException ex) {
				MainGui.writeLogError("ex " + ex.getMessage());
			}

			redrawTable();
		});

		txtSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				table.setModel(
						new DataTableModel(dataBase.getColumNames(), dataBase.getTableData(txtSearch.getText())));
			}
		});

		// insert window dynamic
		JButton btnAdd = new JButton("Add " + dataBase.getTableName().replaceAll("_", " "));

		btnAdd.addActionListener(e -> {

			setEnabled(false);
			JPanel contentPaneWindow;

			JFrame popupWindow = new JFrame();

			popupWindow.setType(Type.UTILITY);
			popupWindow.setResizable(false);
			popupWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

			popupWindow.setTitle("Add " + dataBase.getTableName().replaceAll("_", " "));

			popupWindow.setBounds(100, 100, 251, 446);

			popupWindow.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent arg0) {
					setEnabled(true);
				}
			});

			contentPaneWindow = new JPanel();
			contentPaneWindow.setBorder(new EmptyBorder(5, 5, 5, 5));
			popupWindow.setContentPane(contentPaneWindow);
			contentPaneWindow.setLayout(null);

			JButton btnSave = new JButton("Save");
			btnSave.setBounds(5, 383, 235, 23);

			btnSave.addActionListener(e1 -> {
				Component[] cmp = contentPaneWindow.getComponents();

				HashMap<String, Object> insert = new HashMap<>();

				for (Component comp : cmp) {
					try {
						if (comp.getName().startsWith("txt_") && ((JTextField) comp).getText().length() > 0) {
							insert.put(comp.getName().substring(4), ((JTextField) comp).getText());
						}
					} catch (NullPointerException ex) {
						MainGui.writeLogError("Ex " + ex.getMessage());
					}
				}

				SqlCommands sql = new SqlCommands();
				try {
					sql.sqlInsert(dataBase.getTableName(), insert);
				} catch (SQLException ex) {
					MainGui.writeLogError("Ex " + ex.getMessage());
				}

				redrawTable();
				setEnabled(true);
				popupWindow.dispose();

			});

			contentPaneWindow.add(btnSave);

			LinkedHashMap<String, Object> fields = (LinkedHashMap<String, Object>) dataBase.getColumNames();

			int newRow = 22;
			int currentPos = 11;

			for (String entry : fields.keySet()) {
				if (entry.equals("id")) {
					continue;
				}

				JLabel lblNewLabel = new JLabel(entry);
				lblNewLabel.setBounds(5, currentPos, 92, 14);
				contentPaneWindow.add(lblNewLabel);

				JTextField textField = new JTextField();
				textField.setName("txt_" + entry);
				textField.setBounds(120, currentPos - 3, 120, 20);
				contentPaneWindow.add(textField);
				textField.setColumns(10);

				currentPos += newRow;
			}
			popupWindow.setVisible(true);

		});

		btnAdd.setBounds(185, 4, 200, 23);
		contentPane.add(btnAdd);

	}

	public void redrawTable() {
		Cache.reloadCache();
		table.setModel(new DataTableModel(dataBase.getColumNames(), dataBase.getTableData()));
	}
}
