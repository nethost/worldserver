package org.nethost.handlers;

import java.util.Timer;
import java.util.TimerTask;

import org.nethost.commands.SendPlayerDataResponse;
import org.nethost.globals.Server;
import org.nethost.globals.Variables;

import flexjson.JSONSerializer;

//class that handles server global messages
public class GlobalsSender {
	private GlobalsSender() {
		
	}
	//Sends player position every X seconds
	public static void sendPlayerData(int milliseconds) {
		//initialize instance of timer
		Timer t = new Timer();
		
		//new task for timer
		t.schedule(new TimerTask() {
			@Override
			public void run() {
				//if server is running and we got some connected clients
				if (Variables.getQs() != null && !Variables.getQs().isClosed() && Variables.getConnectedClients().size() > 0) {
					//initialize instance of serializer
					JSONSerializer serializer = new JSONSerializer();
					
					//serialize response to json
					String responseJson = serializer.include("playerData").serialize(new SendPlayerDataResponse());
					
					//broadcast json to all clients
					Server.broadcastToAllClients(responseJson, true);
				}
			}

		}, 0, milliseconds);
	}
}
